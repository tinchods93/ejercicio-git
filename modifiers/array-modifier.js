function arrayModifier(inputArray) {
  let new_array = inputArray.map((elem) => {
    let new_elem = elem.split('');

    //Fisher-Yates algorithm to shuffer an array
    for (let i = new_elem.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = new_elem[i];
      new_elem[i] = new_elem[j];
      new_elem[j] = temp;
    }
    return new_elem.join('');
  });
  return new_array;
}

module.exports = {
  arrayModifier,
};
