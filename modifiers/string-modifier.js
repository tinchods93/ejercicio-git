function stringModifier(inputString) {
  let new_string = inputString.toUpperCase().split('').sort().reverse();
  new_string = new_string.map((character, index) => {
    return index % 2 === 0 ? character.toLowerCase() : character;
  });
  return new_string.join('');
}

module.exports = {
  stringModifier,
};
